package com.techprimers.springbatchexample1.controller;

import org.quartz.SchedulerException;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.techprimers.springbatchexample1.scheduler.SchedulerManager;

@RestController
@RequestMapping("/load")
public class LoadController {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @Autowired
    JobOperator jobOperator;

    @Autowired
    JobExplorer jobExplorer;

    @Autowired
    SchedulerManager schedulerManager;

    @PostMapping
    public String scheduleJob() throws SchedulerException {
        schedulerManager.startScheduler();
        return "SUCCESS";
    }

    @GetMapping
    public BatchStatus load() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {

        int random_int = (int)(Math.random() * (1000 - 10 + 1) + 10);
        System.out.println(random_int);

        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        maps.put("process_id", new JobParameter(random_int+""));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(job, parameters);

        System.out.println("JobExecution: " + jobExecution.getStatus());

        System.out.println("Batch is Running...");
        while (jobExecution.isRunning()) {
            System.out.println("...");
        }
        System.out.println(jobExecution.getJobInstance().getInstanceId());
        System.out.println(jobExecution.getId());
        System.out.println(jobExecution.getJobId());

        return jobExecution.getStatus();
    }

    @GetMapping("/restart/{id}")
    public BatchStatus restart(@PathVariable("id") Integer id) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException,
            JobInstanceAlreadyCompleteException, NoSuchJobExecutionException, NoSuchJobException {
        final Long restartId = jobOperator.restart(id);
        final JobExecution restartExecution = jobExplorer.getJobExecution(restartId);
        return restartExecution.getStatus();
    }
}
