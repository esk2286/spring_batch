package com.techprimers.springbatchexample1.scheduler;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.techprimers.springbatchexample1.scheduler.jobdetail.SchedulerJobDetail;


@Component
public class SchedulerManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerManager.class);

	@Autowired
	private Scheduler scheduler;

	@Autowired
	private SchedulerJobDetail schedulerJobDetail;

	@PostConstruct
	public void startScheduler() throws SchedulerException{
		schedulerJobDetail.scheduleJob(scheduler);
	}

}
