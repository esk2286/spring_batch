package com.techprimers.springbatchexample1.scheduler.jobdetail.impl;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.stereotype.Component;

import com.techprimers.springbatchexample1.scheduler.job.HourlyEmailJob;
import com.techprimers.springbatchexample1.scheduler.jobdetail.SchedulerJobDetail;

@Component
public class SchedulerJobDetailImpl implements SchedulerJobDetail {
	
	public JobDetail buildJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        return JobBuilder.newJob(HourlyEmailJob.class)
                		.withIdentity("test", "test-group")
                		.withDescription("Hourly Email Job")
                		.usingJobData(jobDataMap)
                		.storeDurably()
                		.build();
    }

    public Trigger buildJobTrigger(JobDetail jobDetail) {
    	//"0 * * ? * *" -- minute
        return TriggerBuilder.newTrigger()
                			.forJob(jobDetail)
                			.withIdentity(jobDetail.getKey().getName(), "test-triggers-group")
                			.withDescription("Trigger every minute")
                			.withSchedule(CronScheduleBuilder.cronSchedule("0 * * ? * *").withMisfireHandlingInstructionDoNothing())
                			.build();
    }
}