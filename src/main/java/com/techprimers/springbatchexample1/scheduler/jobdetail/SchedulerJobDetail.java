package com.techprimers.springbatchexample1.scheduler.jobdetail;

import java.util.Date;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

public interface SchedulerJobDetail {
	
	JobDetail buildJobDetail();

    Trigger buildJobTrigger(JobDetail jobDetail);
    
    default Date scheduleJob(Scheduler scheduler) throws SchedulerException {
    	JobDetail jobDetail = buildJobDetail();
    	return scheduler.scheduleJob(jobDetail, buildJobTrigger(jobDetail));
    }
}
