package com.techprimers.springbatchexample1.scheduler.job;

import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class HourlyEmailJob extends QuartzJobBean {
    
	private static final Logger LOGGER = LoggerFactory.getLogger(HourlyEmailJob.class);

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    @Qualifier("csvJob")
    Job csvJob;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
    	LOGGER.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());
        try {
            load();
            // Queue data post -> producer
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BatchStatus load() throws Exception {

        int random_int = (int)(Math.random() * (1000 - 10 + 1) + 10);
        System.out.println(random_int);

        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        maps.put("process_id", new JobParameter(random_int+""));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(csvJob, parameters);

        System.out.println("JobExecution: " + jobExecution.getStatus());

        System.out.println("Batch is Running...");
        while (jobExecution.isRunning()) {
            System.out.println("...");
        }


        return jobExecution.getStatus();
    }


}
